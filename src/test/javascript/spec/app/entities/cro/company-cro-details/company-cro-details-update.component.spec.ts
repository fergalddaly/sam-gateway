/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { GatewayTestModule } from '../../../../test.module';
import { CompanyCroDetailsUpdateComponent } from 'app/entities/cro/company-cro-details/company-cro-details-update.component';
import { CompanyCroDetailsService } from 'app/entities/cro/company-cro-details/company-cro-details.service';
import { CompanyCroDetails } from 'app/shared/model/cro/company-cro-details.model';

describe('Component Tests', () => {
    describe('CompanyCroDetails Management Update Component', () => {
        let comp: CompanyCroDetailsUpdateComponent;
        let fixture: ComponentFixture<CompanyCroDetailsUpdateComponent>;
        let service: CompanyCroDetailsService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [GatewayTestModule],
                declarations: [CompanyCroDetailsUpdateComponent]
            })
                .overrideTemplate(CompanyCroDetailsUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(CompanyCroDetailsUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CompanyCroDetailsService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new CompanyCroDetails(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.companyCroDetails = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new CompanyCroDetails();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.companyCroDetails = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
