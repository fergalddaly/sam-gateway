/* tslint:disable max-line-length */
import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { take, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { CompanyCroDetailsService } from 'app/entities/cro/company-cro-details/company-cro-details.service';
import { ICompanyCroDetails, CompanyCroDetails } from 'app/shared/model/cro/company-cro-details.model';

describe('Service Tests', () => {
    describe('CompanyCroDetails Service', () => {
        let injector: TestBed;
        let service: CompanyCroDetailsService;
        let httpMock: HttpTestingController;
        let elemDefault: ICompanyCroDetails;
        let currentDate: moment.Moment;
        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [HttpClientTestingModule]
            });
            injector = getTestBed();
            service = injector.get(CompanyCroDetailsService);
            httpMock = injector.get(HttpTestingController);
            currentDate = moment();

            elemDefault = new CompanyCroDetails(
                0,
                'AAAAAAA',
                0,
                0,
                'AAAAAAA',
                currentDate,
                0,
                currentDate,
                'AAAAAAA',
                'AAAAAAA',
                'AAAAAAA',
                'AAAAAAA',
                'AAAAAAA',
                'AAAAAAA',
                'AAAAAAA',
                currentDate,
                currentDate,
                currentDate
            );
        });

        describe('Service methods', async () => {
            it('should find an element', async () => {
                const returnedFromService = Object.assign(
                    {
                        companyStatusDate: currentDate.format(DATE_FORMAT),
                        companyRegDate: currentDate.format(DATE_FORMAT),
                        lastArDate: currentDate.format(DATE_FORMAT),
                        nextArDate: currentDate.format(DATE_FORMAT),
                        lastAccDate: currentDate.format(DATE_FORMAT)
                    },
                    elemDefault
                );
                service
                    .find(123)
                    .pipe(take(1))
                    .subscribe(resp => expect(resp).toMatchObject({ body: elemDefault }));

                const req = httpMock.expectOne({ method: 'GET' });
                req.flush(JSON.stringify(returnedFromService));
            });

            it('should create a CompanyCroDetails', async () => {
                const returnedFromService = Object.assign(
                    {
                        id: 0,
                        companyStatusDate: currentDate.format(DATE_FORMAT),
                        companyRegDate: currentDate.format(DATE_FORMAT),
                        lastArDate: currentDate.format(DATE_FORMAT),
                        nextArDate: currentDate.format(DATE_FORMAT),
                        lastAccDate: currentDate.format(DATE_FORMAT)
                    },
                    elemDefault
                );
                const expected = Object.assign(
                    {
                        companyStatusDate: currentDate,
                        companyRegDate: currentDate,
                        lastArDate: currentDate,
                        nextArDate: currentDate,
                        lastAccDate: currentDate
                    },
                    returnedFromService
                );
                service
                    .create(new CompanyCroDetails(null))
                    .pipe(take(1))
                    .subscribe(resp => expect(resp).toMatchObject({ body: expected }));
                const req = httpMock.expectOne({ method: 'POST' });
                req.flush(JSON.stringify(returnedFromService));
            });

            it('should update a CompanyCroDetails', async () => {
                const returnedFromService = Object.assign(
                    {
                        companyName: 'BBBBBB',
                        companyNumber: 1,
                        companyStatusCode: 1,
                        companyStatusDesc: 'BBBBBB',
                        companyStatusDate: currentDate.format(DATE_FORMAT),
                        companyTypeCode: 1,
                        companyRegDate: currentDate.format(DATE_FORMAT),
                        compTypeDesc: 'BBBBBB',
                        placeOfBusiness: 'BBBBBB',
                        eircode: 'BBBBBB',
                        companyAddr1: 'BBBBBB',
                        companyAddr2: 'BBBBBB',
                        companyAddr3: 'BBBBBB',
                        companyAddr4: 'BBBBBB',
                        lastArDate: currentDate.format(DATE_FORMAT),
                        nextArDate: currentDate.format(DATE_FORMAT),
                        lastAccDate: currentDate.format(DATE_FORMAT)
                    },
                    elemDefault
                );

                const expected = Object.assign(
                    {
                        companyStatusDate: currentDate,
                        companyRegDate: currentDate,
                        lastArDate: currentDate,
                        nextArDate: currentDate,
                        lastAccDate: currentDate
                    },
                    returnedFromService
                );
                service
                    .update(expected)
                    .pipe(take(1))
                    .subscribe(resp => expect(resp).toMatchObject({ body: expected }));
                const req = httpMock.expectOne({ method: 'PUT' });
                req.flush(JSON.stringify(returnedFromService));
            });

            it('should return a list of CompanyCroDetails', async () => {
                const returnedFromService = Object.assign(
                    {
                        companyName: 'BBBBBB',
                        companyNumber: 1,
                        companyStatusCode: 1,
                        companyStatusDesc: 'BBBBBB',
                        companyStatusDate: currentDate.format(DATE_FORMAT),
                        companyTypeCode: 1,
                        companyRegDate: currentDate.format(DATE_FORMAT),
                        compTypeDesc: 'BBBBBB',
                        placeOfBusiness: 'BBBBBB',
                        eircode: 'BBBBBB',
                        companyAddr1: 'BBBBBB',
                        companyAddr2: 'BBBBBB',
                        companyAddr3: 'BBBBBB',
                        companyAddr4: 'BBBBBB',
                        lastArDate: currentDate.format(DATE_FORMAT),
                        nextArDate: currentDate.format(DATE_FORMAT),
                        lastAccDate: currentDate.format(DATE_FORMAT)
                    },
                    elemDefault
                );
                const expected = Object.assign(
                    {
                        companyStatusDate: currentDate,
                        companyRegDate: currentDate,
                        lastArDate: currentDate,
                        nextArDate: currentDate,
                        lastAccDate: currentDate
                    },
                    returnedFromService
                );
                service
                    .query(expected)
                    .pipe(take(1), map(resp => resp.body))
                    .subscribe(body => expect(body).toContainEqual(expected));
                const req = httpMock.expectOne({ method: 'GET' });
                req.flush(JSON.stringify([returnedFromService]));
                httpMock.verify();
            });

            it('should delete a CompanyCroDetails', async () => {
                const rxPromise = service.delete(123).subscribe(resp => expect(resp.ok));

                const req = httpMock.expectOne({ method: 'DELETE' });
                req.flush({ status: 200 });
            });
        });

        afterEach(() => {
            httpMock.verify();
        });
    });
});
