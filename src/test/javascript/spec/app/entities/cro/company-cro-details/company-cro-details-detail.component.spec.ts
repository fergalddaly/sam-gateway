/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { GatewayTestModule } from '../../../../test.module';
import { CompanyCroDetailsDetailComponent } from 'app/entities/cro/company-cro-details/company-cro-details-detail.component';
import { CompanyCroDetails } from 'app/shared/model/cro/company-cro-details.model';

describe('Component Tests', () => {
    describe('CompanyCroDetails Management Detail Component', () => {
        let comp: CompanyCroDetailsDetailComponent;
        let fixture: ComponentFixture<CompanyCroDetailsDetailComponent>;
        const route = ({ data: of({ companyCroDetails: new CompanyCroDetails(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [GatewayTestModule],
                declarations: [CompanyCroDetailsDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(CompanyCroDetailsDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(CompanyCroDetailsDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.companyCroDetails).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
