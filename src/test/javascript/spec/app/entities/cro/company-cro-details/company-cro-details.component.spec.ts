/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { GatewayTestModule } from '../../../../test.module';
import { CompanyCroDetailsComponent } from 'app/entities/cro/company-cro-details/company-cro-details.component';
import { CompanyCroDetailsService } from 'app/entities/cro/company-cro-details/company-cro-details.service';
import { CompanyCroDetails } from 'app/shared/model/cro/company-cro-details.model';

describe('Component Tests', () => {
    describe('CompanyCroDetails Management Component', () => {
        let comp: CompanyCroDetailsComponent;
        let fixture: ComponentFixture<CompanyCroDetailsComponent>;
        let service: CompanyCroDetailsService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [GatewayTestModule],
                declarations: [CompanyCroDetailsComponent],
                providers: []
            })
                .overrideTemplate(CompanyCroDetailsComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(CompanyCroDetailsComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CompanyCroDetailsService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new CompanyCroDetails(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.companyCroDetails[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
