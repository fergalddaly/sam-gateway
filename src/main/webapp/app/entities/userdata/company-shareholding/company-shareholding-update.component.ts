import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';

import { ICompanyShareholding } from 'app/shared/model/userdata/company-shareholding.model';
import { CompanyShareholdingService } from './company-shareholding.service';
import { ICompany } from 'app/shared/model/userdata/company.model';
import { CompanyService } from 'app/entities/userdata/company';
import { IPerson } from 'app/shared/model/userdata/person.model';
import { PersonService } from 'app/entities/userdata/person';

@Component({
    selector: 'jhi-company-shareholding-update',
    templateUrl: './company-shareholding-update.component.html'
})
export class CompanyShareholdingUpdateComponent implements OnInit {
    companyShareholding: ICompanyShareholding;
    isSaving: boolean;

    companies: ICompany[];

    people: IPerson[];

    constructor(
        private jhiAlertService: JhiAlertService,
        private companyShareholdingService: CompanyShareholdingService,
        private companyService: CompanyService,
        private personService: PersonService,
        private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ companyShareholding }) => {
            this.companyShareholding = companyShareholding;
        });
        this.companyService.query().subscribe(
            (res: HttpResponse<ICompany[]>) => {
                this.companies = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
        this.personService.query().subscribe(
            (res: HttpResponse<IPerson[]>) => {
                this.people = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.companyShareholding.id !== undefined) {
            this.subscribeToSaveResponse(this.companyShareholdingService.update(this.companyShareholding));
        } else {
            this.subscribeToSaveResponse(this.companyShareholdingService.create(this.companyShareholding));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<ICompanyShareholding>>) {
        result.subscribe((res: HttpResponse<ICompanyShareholding>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackCompanyById(index: number, item: ICompany) {
        return item.id;
    }

    trackPersonById(index: number, item: IPerson) {
        return item.id;
    }
}
