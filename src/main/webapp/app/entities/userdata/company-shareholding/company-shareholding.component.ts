import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ICompanyShareholding } from 'app/shared/model/userdata/company-shareholding.model';
import { Principal } from 'app/core';
import { CompanyShareholdingService } from './company-shareholding.service';

@Component({
    selector: 'jhi-company-shareholding',
    templateUrl: './company-shareholding.component.html'
})
export class CompanyShareholdingComponent implements OnInit, OnDestroy {
    companyShareholdings: ICompanyShareholding[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private companyShareholdingService: CompanyShareholdingService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {}

    loadAll() {
        this.companyShareholdingService.query().subscribe(
            (res: HttpResponse<ICompanyShareholding[]>) => {
                this.companyShareholdings = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInCompanyShareholdings();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: ICompanyShareholding) {
        return item.id;
    }

    registerChangeInCompanyShareholdings() {
        this.eventSubscriber = this.eventManager.subscribe('companyShareholdingListModification', response => this.loadAll());
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
