import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { CompanyShareholding } from 'app/shared/model/userdata/company-shareholding.model';
import { CompanyShareholdingService } from './company-shareholding.service';
import { CompanyShareholdingComponent } from './company-shareholding.component';
import { CompanyShareholdingDetailComponent } from './company-shareholding-detail.component';
import { CompanyShareholdingUpdateComponent } from './company-shareholding-update.component';
import { CompanyShareholdingDeletePopupComponent } from './company-shareholding-delete-dialog.component';
import { ICompanyShareholding } from 'app/shared/model/userdata/company-shareholding.model';

@Injectable({ providedIn: 'root' })
export class CompanyShareholdingResolve implements Resolve<ICompanyShareholding> {
    constructor(private service: CompanyShareholdingService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((companyShareholding: HttpResponse<CompanyShareholding>) => companyShareholding.body));
        }
        return of(new CompanyShareholding());
    }
}

export const companyShareholdingRoute: Routes = [
    {
        path: 'company-shareholding',
        component: CompanyShareholdingComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'CompanyShareholdings'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'company-shareholding/:id/view',
        component: CompanyShareholdingDetailComponent,
        resolve: {
            companyShareholding: CompanyShareholdingResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'CompanyShareholdings'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'company-shareholding/new',
        component: CompanyShareholdingUpdateComponent,
        resolve: {
            companyShareholding: CompanyShareholdingResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'CompanyShareholdings'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'company-shareholding/:id/edit',
        component: CompanyShareholdingUpdateComponent,
        resolve: {
            companyShareholding: CompanyShareholdingResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'CompanyShareholdings'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const companyShareholdingPopupRoute: Routes = [
    {
        path: 'company-shareholding/:id/delete',
        component: CompanyShareholdingDeletePopupComponent,
        resolve: {
            companyShareholding: CompanyShareholdingResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'CompanyShareholdings'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
