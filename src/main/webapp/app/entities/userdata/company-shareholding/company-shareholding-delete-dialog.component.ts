import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ICompanyShareholding } from 'app/shared/model/userdata/company-shareholding.model';
import { CompanyShareholdingService } from './company-shareholding.service';

@Component({
    selector: 'jhi-company-shareholding-delete-dialog',
    templateUrl: './company-shareholding-delete-dialog.component.html'
})
export class CompanyShareholdingDeleteDialogComponent {
    companyShareholding: ICompanyShareholding;

    constructor(
        private companyShareholdingService: CompanyShareholdingService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.companyShareholdingService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'companyShareholdingListModification',
                content: 'Deleted an companyShareholding'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-company-shareholding-delete-popup',
    template: ''
})
export class CompanyShareholdingDeletePopupComponent implements OnInit, OnDestroy {
    private ngbModalRef: NgbModalRef;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ companyShareholding }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(CompanyShareholdingDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.companyShareholding = companyShareholding;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
