import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ICompanyShareholding } from 'app/shared/model/userdata/company-shareholding.model';

type EntityResponseType = HttpResponse<ICompanyShareholding>;
type EntityArrayResponseType = HttpResponse<ICompanyShareholding[]>;

@Injectable({ providedIn: 'root' })
export class CompanyShareholdingService {
    public resourceUrl = SERVER_API_URL + 'userdata/api/company-shareholdings';

    constructor(private http: HttpClient) {}

    create(companyShareholding: ICompanyShareholding): Observable<EntityResponseType> {
        return this.http.post<ICompanyShareholding>(this.resourceUrl, companyShareholding, { observe: 'response' });
    }

    update(companyShareholding: ICompanyShareholding): Observable<EntityResponseType> {
        return this.http.put<ICompanyShareholding>(this.resourceUrl, companyShareholding, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<ICompanyShareholding>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<ICompanyShareholding[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
