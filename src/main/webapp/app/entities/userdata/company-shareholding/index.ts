export * from './company-shareholding.service';
export * from './company-shareholding-update.component';
export * from './company-shareholding-delete-dialog.component';
export * from './company-shareholding-detail.component';
export * from './company-shareholding.component';
export * from './company-shareholding.route';
