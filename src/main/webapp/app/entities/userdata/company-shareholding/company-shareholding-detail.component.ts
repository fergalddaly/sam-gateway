import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ICompanyShareholding } from 'app/shared/model/userdata/company-shareholding.model';

@Component({
    selector: 'jhi-company-shareholding-detail',
    templateUrl: './company-shareholding-detail.component.html'
})
export class CompanyShareholdingDetailComponent implements OnInit {
    companyShareholding: ICompanyShareholding;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ companyShareholding }) => {
            this.companyShareholding = companyShareholding;
        });
    }

    previousState() {
        window.history.back();
    }
}
