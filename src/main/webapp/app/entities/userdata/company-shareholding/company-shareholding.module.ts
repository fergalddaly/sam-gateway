import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GatewaySharedModule } from 'app/shared';
import {
    CompanyShareholdingComponent,
    CompanyShareholdingDetailComponent,
    CompanyShareholdingUpdateComponent,
    CompanyShareholdingDeletePopupComponent,
    CompanyShareholdingDeleteDialogComponent,
    companyShareholdingRoute,
    companyShareholdingPopupRoute
} from './';

const ENTITY_STATES = [...companyShareholdingRoute, ...companyShareholdingPopupRoute];

@NgModule({
    imports: [GatewaySharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        CompanyShareholdingComponent,
        CompanyShareholdingDetailComponent,
        CompanyShareholdingUpdateComponent,
        CompanyShareholdingDeleteDialogComponent,
        CompanyShareholdingDeletePopupComponent
    ],
    entryComponents: [
        CompanyShareholdingComponent,
        CompanyShareholdingUpdateComponent,
        CompanyShareholdingDeleteDialogComponent,
        CompanyShareholdingDeletePopupComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GatewayCompanyShareholdingModule {}
