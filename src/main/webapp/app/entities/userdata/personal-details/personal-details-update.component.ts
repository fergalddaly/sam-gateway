import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';

import { IPersonalDetails } from 'app/shared/model/userdata/personal-details.model';
import { PersonalDetailsService } from './personal-details.service';
import { IPerson } from 'app/shared/model/userdata/person.model';
import { PersonService } from 'app/entities/userdata/person';

@Component({
    selector: 'jhi-personal-details-update',
    templateUrl: './personal-details-update.component.html'
})
export class PersonalDetailsUpdateComponent implements OnInit {
    personalDetails: IPersonalDetails;
    isSaving: boolean;

    people: IPerson[];

    constructor(
        private jhiAlertService: JhiAlertService,
        private personalDetailsService: PersonalDetailsService,
        private personService: PersonService,
        private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ personalDetails }) => {
            this.personalDetails = personalDetails;
        });
        this.personService.query({ 'personalDetailsId.specified': 'false' }).subscribe(
            (res: HttpResponse<IPerson[]>) => {
                if (!this.personalDetails.person || !this.personalDetails.person.id) {
                    this.people = res.body;
                } else {
                    this.personService.find(this.personalDetails.person.id).subscribe(
                        (subRes: HttpResponse<IPerson>) => {
                            this.people = [subRes.body].concat(res.body);
                        },
                        (subRes: HttpErrorResponse) => this.onError(subRes.message)
                    );
                }
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.personalDetails.id !== undefined) {
            this.subscribeToSaveResponse(this.personalDetailsService.update(this.personalDetails));
        } else {
            this.subscribeToSaveResponse(this.personalDetailsService.create(this.personalDetails));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IPersonalDetails>>) {
        result.subscribe((res: HttpResponse<IPersonalDetails>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackPersonById(index: number, item: IPerson) {
        return item.id;
    }
}
