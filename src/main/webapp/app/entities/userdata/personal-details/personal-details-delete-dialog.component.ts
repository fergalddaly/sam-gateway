import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IPersonalDetails } from 'app/shared/model/userdata/personal-details.model';
import { PersonalDetailsService } from './personal-details.service';

@Component({
    selector: 'jhi-personal-details-delete-dialog',
    templateUrl: './personal-details-delete-dialog.component.html'
})
export class PersonalDetailsDeleteDialogComponent {
    personalDetails: IPersonalDetails;

    constructor(
        private personalDetailsService: PersonalDetailsService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.personalDetailsService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'personalDetailsListModification',
                content: 'Deleted an personalDetails'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-personal-details-delete-popup',
    template: ''
})
export class PersonalDetailsDeletePopupComponent implements OnInit, OnDestroy {
    private ngbModalRef: NgbModalRef;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ personalDetails }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(PersonalDetailsDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.personalDetails = personalDetails;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
