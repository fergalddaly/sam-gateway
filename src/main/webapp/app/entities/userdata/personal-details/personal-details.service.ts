import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IPersonalDetails } from 'app/shared/model/userdata/personal-details.model';

type EntityResponseType = HttpResponse<IPersonalDetails>;
type EntityArrayResponseType = HttpResponse<IPersonalDetails[]>;

@Injectable({ providedIn: 'root' })
export class PersonalDetailsService {
    public resourceUrl = SERVER_API_URL + 'userdata/api/personal-details';

    constructor(private http: HttpClient) {}

    create(personalDetails: IPersonalDetails): Observable<EntityResponseType> {
        return this.http.post<IPersonalDetails>(this.resourceUrl, personalDetails, { observe: 'response' });
    }

    update(personalDetails: IPersonalDetails): Observable<EntityResponseType> {
        return this.http.put<IPersonalDetails>(this.resourceUrl, personalDetails, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IPersonalDetails>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IPersonalDetails[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
