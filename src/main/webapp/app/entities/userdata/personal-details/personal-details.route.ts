import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { PersonalDetails } from 'app/shared/model/userdata/personal-details.model';
import { PersonalDetailsService } from './personal-details.service';
import { PersonalDetailsComponent } from './personal-details.component';
import { PersonalDetailsDetailComponent } from './personal-details-detail.component';
import { PersonalDetailsUpdateComponent } from './personal-details-update.component';
import { PersonalDetailsDeletePopupComponent } from './personal-details-delete-dialog.component';
import { IPersonalDetails } from 'app/shared/model/userdata/personal-details.model';

@Injectable({ providedIn: 'root' })
export class PersonalDetailsResolve implements Resolve<IPersonalDetails> {
    constructor(private service: PersonalDetailsService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((personalDetails: HttpResponse<PersonalDetails>) => personalDetails.body));
        }
        return of(new PersonalDetails());
    }
}

export const personalDetailsRoute: Routes = [
    {
        path: 'personal-details',
        component: PersonalDetailsComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'PersonalDetails'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'personal-details/:id/view',
        component: PersonalDetailsDetailComponent,
        resolve: {
            personalDetails: PersonalDetailsResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'PersonalDetails'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'personal-details/new',
        component: PersonalDetailsUpdateComponent,
        resolve: {
            personalDetails: PersonalDetailsResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'PersonalDetails'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'personal-details/:id/edit',
        component: PersonalDetailsUpdateComponent,
        resolve: {
            personalDetails: PersonalDetailsResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'PersonalDetails'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const personalDetailsPopupRoute: Routes = [
    {
        path: 'personal-details/:id/delete',
        component: PersonalDetailsDeletePopupComponent,
        resolve: {
            personalDetails: PersonalDetailsResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'PersonalDetails'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
