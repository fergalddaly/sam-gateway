import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GatewaySharedModule } from 'app/shared';
import {
    PersonalDetailsComponent,
    PersonalDetailsDetailComponent,
    PersonalDetailsUpdateComponent,
    PersonalDetailsDeletePopupComponent,
    PersonalDetailsDeleteDialogComponent,
    personalDetailsRoute,
    personalDetailsPopupRoute
} from './';

const ENTITY_STATES = [...personalDetailsRoute, ...personalDetailsPopupRoute];

@NgModule({
    imports: [GatewaySharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        PersonalDetailsComponent,
        PersonalDetailsDetailComponent,
        PersonalDetailsUpdateComponent,
        PersonalDetailsDeleteDialogComponent,
        PersonalDetailsDeletePopupComponent
    ],
    entryComponents: [
        PersonalDetailsComponent,
        PersonalDetailsUpdateComponent,
        PersonalDetailsDeleteDialogComponent,
        PersonalDetailsDeletePopupComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GatewayPersonalDetailsModule {}
