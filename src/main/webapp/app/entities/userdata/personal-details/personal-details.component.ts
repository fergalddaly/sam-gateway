import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IPersonalDetails } from 'app/shared/model/userdata/personal-details.model';
import { Principal } from 'app/core';
import { PersonalDetailsService } from './personal-details.service';

@Component({
    selector: 'jhi-personal-details',
    templateUrl: './personal-details.component.html'
})
export class PersonalDetailsComponent implements OnInit, OnDestroy {
    personalDetails: IPersonalDetails[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private personalDetailsService: PersonalDetailsService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {}

    loadAll() {
        this.personalDetailsService.query().subscribe(
            (res: HttpResponse<IPersonalDetails[]>) => {
                this.personalDetails = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInPersonalDetails();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IPersonalDetails) {
        return item.id;
    }

    registerChangeInPersonalDetails() {
        this.eventSubscriber = this.eventManager.subscribe('personalDetailsListModification', response => this.loadAll());
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
