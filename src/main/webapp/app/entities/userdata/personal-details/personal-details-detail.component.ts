import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IPersonalDetails } from 'app/shared/model/userdata/personal-details.model';

@Component({
    selector: 'jhi-personal-details-detail',
    templateUrl: './personal-details-detail.component.html'
})
export class PersonalDetailsDetailComponent implements OnInit {
    personalDetails: IPersonalDetails;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ personalDetails }) => {
            this.personalDetails = personalDetails;
        });
    }

    previousState() {
        window.history.back();
    }
}
