export * from './personal-details.service';
export * from './personal-details-update.component';
export * from './personal-details-delete-dialog.component';
export * from './personal-details-detail.component';
export * from './personal-details.component';
export * from './personal-details.route';
