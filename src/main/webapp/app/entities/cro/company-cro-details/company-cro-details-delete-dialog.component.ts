import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ICompanyCroDetails } from 'app/shared/model/cro/company-cro-details.model';
import { CompanyCroDetailsService } from './company-cro-details.service';

@Component({
    selector: 'jhi-company-cro-details-delete-dialog',
    templateUrl: './company-cro-details-delete-dialog.component.html'
})
export class CompanyCroDetailsDeleteDialogComponent {
    companyCroDetails: ICompanyCroDetails;

    constructor(
        private companyCroDetailsService: CompanyCroDetailsService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.companyCroDetailsService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'companyCroDetailsListModification',
                content: 'Deleted an companyCroDetails'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-company-cro-details-delete-popup',
    template: ''
})
export class CompanyCroDetailsDeletePopupComponent implements OnInit, OnDestroy {
    private ngbModalRef: NgbModalRef;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ companyCroDetails }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(CompanyCroDetailsDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.companyCroDetails = companyCroDetails;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
