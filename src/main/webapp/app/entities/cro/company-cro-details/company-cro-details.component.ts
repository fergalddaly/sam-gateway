import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ICompanyCroDetails } from 'app/shared/model/cro/company-cro-details.model';
import { Principal } from 'app/core';
import { CompanyCroDetailsService } from './company-cro-details.service';

@Component({
    selector: 'jhi-company-cro-details',
    templateUrl: './company-cro-details.component.html'
})
export class CompanyCroDetailsComponent implements OnInit, OnDestroy {
    companyCroDetails: ICompanyCroDetails[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private companyCroDetailsService: CompanyCroDetailsService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {}

    loadAll() {
        this.companyCroDetailsService.query().subscribe(
            (res: HttpResponse<ICompanyCroDetails[]>) => {
                this.companyCroDetails = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInCompanyCroDetails();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: ICompanyCroDetails) {
        return item.id;
    }

    registerChangeInCompanyCroDetails() {
        this.eventSubscriber = this.eventManager.subscribe('companyCroDetailsListModification', response => this.loadAll());
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
