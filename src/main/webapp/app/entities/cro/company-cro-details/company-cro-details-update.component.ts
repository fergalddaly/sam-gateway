import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';

import { ICompanyCroDetails } from 'app/shared/model/cro/company-cro-details.model';
import { CompanyCroDetailsService } from './company-cro-details.service';

@Component({
    selector: 'jhi-company-cro-details-update',
    templateUrl: './company-cro-details-update.component.html'
})
export class CompanyCroDetailsUpdateComponent implements OnInit {
    companyCroDetails: ICompanyCroDetails;
    isSaving: boolean;
    companyStatusDateDp: any;
    companyRegDateDp: any;
    lastArDateDp: any;
    nextArDateDp: any;
    lastAccDateDp: any;

    constructor(private companyCroDetailsService: CompanyCroDetailsService, private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ companyCroDetails }) => {
            this.companyCroDetails = companyCroDetails;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.companyCroDetails.id !== undefined) {
            this.subscribeToSaveResponse(this.companyCroDetailsService.update(this.companyCroDetails));
        } else {
            this.subscribeToSaveResponse(this.companyCroDetailsService.create(this.companyCroDetails));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<ICompanyCroDetails>>) {
        result.subscribe((res: HttpResponse<ICompanyCroDetails>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }
}
