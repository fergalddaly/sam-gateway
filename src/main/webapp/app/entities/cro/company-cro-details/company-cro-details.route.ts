import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { CompanyCroDetails } from 'app/shared/model/cro/company-cro-details.model';
import { CompanyCroDetailsService } from './company-cro-details.service';
import { CompanyCroDetailsComponent } from './company-cro-details.component';
import { CompanyCroDetailsDetailComponent } from './company-cro-details-detail.component';
import { CompanyCroDetailsUpdateComponent } from './company-cro-details-update.component';
import { CompanyCroDetailsDeletePopupComponent } from './company-cro-details-delete-dialog.component';
import { ICompanyCroDetails } from 'app/shared/model/cro/company-cro-details.model';

@Injectable({ providedIn: 'root' })
export class CompanyCroDetailsResolve implements Resolve<ICompanyCroDetails> {
    constructor(private service: CompanyCroDetailsService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((companyCroDetails: HttpResponse<CompanyCroDetails>) => companyCroDetails.body));
        }
        return of(new CompanyCroDetails());
    }
}

export const companyCroDetailsRoute: Routes = [
    {
        path: 'company-cro-details',
        component: CompanyCroDetailsComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'CompanyCroDetails'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'company-cro-details/:id/view',
        component: CompanyCroDetailsDetailComponent,
        resolve: {
            companyCroDetails: CompanyCroDetailsResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'CompanyCroDetails'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'company-cro-details/new',
        component: CompanyCroDetailsUpdateComponent,
        resolve: {
            companyCroDetails: CompanyCroDetailsResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'CompanyCroDetails'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'company-cro-details/:id/edit',
        component: CompanyCroDetailsUpdateComponent,
        resolve: {
            companyCroDetails: CompanyCroDetailsResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'CompanyCroDetails'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const companyCroDetailsPopupRoute: Routes = [
    {
        path: 'company-cro-details/:id/delete',
        component: CompanyCroDetailsDeletePopupComponent,
        resolve: {
            companyCroDetails: CompanyCroDetailsResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'CompanyCroDetails'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
