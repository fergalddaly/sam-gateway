import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ICompanyCroDetails } from 'app/shared/model/cro/company-cro-details.model';

type EntityResponseType = HttpResponse<ICompanyCroDetails>;
type EntityArrayResponseType = HttpResponse<ICompanyCroDetails[]>;

@Injectable({ providedIn: 'root' })
export class CompanyCroDetailsService {
    public resourceUrl = SERVER_API_URL + 'cro/api/company-cro-details';

    constructor(private http: HttpClient) {}

    create(companyCroDetails: ICompanyCroDetails): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(companyCroDetails);
        return this.http
            .post<ICompanyCroDetails>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    update(companyCroDetails: ICompanyCroDetails): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(companyCroDetails);
        return this.http
            .put<ICompanyCroDetails>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http
            .get<ICompanyCroDetails>(`${this.resourceUrl}/${id}`, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http
            .get<ICompanyCroDetails[]>(this.resourceUrl, { params: options, observe: 'response' })
            .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    private convertDateFromClient(companyCroDetails: ICompanyCroDetails): ICompanyCroDetails {
        const copy: ICompanyCroDetails = Object.assign({}, companyCroDetails, {
            companyStatusDate:
                companyCroDetails.companyStatusDate != null && companyCroDetails.companyStatusDate.isValid()
                    ? companyCroDetails.companyStatusDate.format(DATE_FORMAT)
                    : null,
            companyRegDate:
                companyCroDetails.companyRegDate != null && companyCroDetails.companyRegDate.isValid()
                    ? companyCroDetails.companyRegDate.format(DATE_FORMAT)
                    : null,
            lastArDate:
                companyCroDetails.lastArDate != null && companyCroDetails.lastArDate.isValid()
                    ? companyCroDetails.lastArDate.format(DATE_FORMAT)
                    : null,
            nextArDate:
                companyCroDetails.nextArDate != null && companyCroDetails.nextArDate.isValid()
                    ? companyCroDetails.nextArDate.format(DATE_FORMAT)
                    : null,
            lastAccDate:
                companyCroDetails.lastAccDate != null && companyCroDetails.lastAccDate.isValid()
                    ? companyCroDetails.lastAccDate.format(DATE_FORMAT)
                    : null
        });
        return copy;
    }

    private convertDateFromServer(res: EntityResponseType): EntityResponseType {
        res.body.companyStatusDate = res.body.companyStatusDate != null ? moment(res.body.companyStatusDate) : null;
        res.body.companyRegDate = res.body.companyRegDate != null ? moment(res.body.companyRegDate) : null;
        res.body.lastArDate = res.body.lastArDate != null ? moment(res.body.lastArDate) : null;
        res.body.nextArDate = res.body.nextArDate != null ? moment(res.body.nextArDate) : null;
        res.body.lastAccDate = res.body.lastAccDate != null ? moment(res.body.lastAccDate) : null;
        return res;
    }

    private convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
        res.body.forEach((companyCroDetails: ICompanyCroDetails) => {
            companyCroDetails.companyStatusDate =
                companyCroDetails.companyStatusDate != null ? moment(companyCroDetails.companyStatusDate) : null;
            companyCroDetails.companyRegDate = companyCroDetails.companyRegDate != null ? moment(companyCroDetails.companyRegDate) : null;
            companyCroDetails.lastArDate = companyCroDetails.lastArDate != null ? moment(companyCroDetails.lastArDate) : null;
            companyCroDetails.nextArDate = companyCroDetails.nextArDate != null ? moment(companyCroDetails.nextArDate) : null;
            companyCroDetails.lastAccDate = companyCroDetails.lastAccDate != null ? moment(companyCroDetails.lastAccDate) : null;
        });
        return res;
    }
}
