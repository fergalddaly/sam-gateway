export * from './company-cro-details.service';
export * from './company-cro-details-update.component';
export * from './company-cro-details-delete-dialog.component';
export * from './company-cro-details-detail.component';
export * from './company-cro-details.component';
export * from './company-cro-details.route';
