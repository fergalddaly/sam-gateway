import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ICompanyCroDetails } from 'app/shared/model/cro/company-cro-details.model';

@Component({
    selector: 'jhi-company-cro-details-detail',
    templateUrl: './company-cro-details-detail.component.html'
})
export class CompanyCroDetailsDetailComponent implements OnInit {
    companyCroDetails: ICompanyCroDetails;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ companyCroDetails }) => {
            this.companyCroDetails = companyCroDetails;
        });
    }

    previousState() {
        window.history.back();
    }
}
