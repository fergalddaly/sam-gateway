import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GatewaySharedModule } from 'app/shared';
import {
    CompanyCroDetailsComponent,
    CompanyCroDetailsDetailComponent,
    CompanyCroDetailsUpdateComponent,
    CompanyCroDetailsDeletePopupComponent,
    CompanyCroDetailsDeleteDialogComponent,
    companyCroDetailsRoute,
    companyCroDetailsPopupRoute
} from './';

const ENTITY_STATES = [...companyCroDetailsRoute, ...companyCroDetailsPopupRoute];

@NgModule({
    imports: [GatewaySharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        CompanyCroDetailsComponent,
        CompanyCroDetailsDetailComponent,
        CompanyCroDetailsUpdateComponent,
        CompanyCroDetailsDeleteDialogComponent,
        CompanyCroDetailsDeletePopupComponent
    ],
    entryComponents: [
        CompanyCroDetailsComponent,
        CompanyCroDetailsUpdateComponent,
        CompanyCroDetailsDeleteDialogComponent,
        CompanyCroDetailsDeletePopupComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GatewayCompanyCroDetailsModule {}
