import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { GatewayPersonalDetailsModule as UserdataPersonalDetailsModule } from './userdata/personal-details/personal-details.module';
import { GatewayPersonModule as UserdataPersonModule } from './userdata/person/person.module';
import { GatewayCompanyModule as UserdataCompanyModule } from './userdata/company/company.module';
import { GatewayCompanyShareholdingModule as UserdataCompanyShareholdingModule } from './userdata/company-shareholding/company-shareholding.module';
import { GatewayCompanyCroDetailsModule as CroCompanyCroDetailsModule } from './cro/company-cro-details/company-cro-details.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    // prettier-ignore
    imports: [
        UserdataPersonalDetailsModule,
        UserdataCompanyModule,
        UserdataCompanyShareholdingModule,
        UserdataPersonModule,
        CroCompanyCroDetailsModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GatewayEntityModule {}
