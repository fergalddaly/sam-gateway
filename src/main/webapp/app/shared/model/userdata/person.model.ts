import { Moment } from 'moment';

export interface IPerson {
    id?: number;
    firstName?: string;
    surname?: string;
    dateOfBirth?: Moment;
    mobileNumber?: number;
}

export class Person implements IPerson {
    constructor(
        public id?: number,
        public firstName?: string,
        public surname?: string,
        public dateOfBirth?: Moment,
        public mobileNumber?: number
    ) {}
}
