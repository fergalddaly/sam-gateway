import { IPerson } from 'app/shared/model/userdata/person.model';
import { ICompanyShareholding } from 'app/shared/model/userdata/company-shareholding.model';

export const enum CompanyType {
    UMBRELLA_LTD = 'UMBRELLA_LTD',
    PRIVATE_LTD = 'PRIVATE_LTD'
}

export interface ICompany {
    id?: number;
    userId?: number;
    name?: string;
    address1?: string;
    address2?: string;
    address3?: string;
    address4?: string;
    registeredVatNumber?: string;
    yearEndDay?: number;
    yearEndMonth?: number;
    shortName?: string;
    type?: CompanyType;
    companyCroDetailsId?: number;
    secretary?: IPerson;
    companyShareholdings?: ICompanyShareholding[];
}

export class Company implements ICompany {
    constructor(
        public id?: number,
        public userId?: number,
        public name?: string,
        public address1?: string,
        public address2?: string,
        public address3?: string,
        public address4?: string,
        public registeredVatNumber?: string,
        public yearEndDay?: number,
        public yearEndMonth?: number,
        public shortName?: string,
        public type?: CompanyType,
        public companyCroDetailsId?: number,
        public secretary?: IPerson,
        public companyShareholdings?: ICompanyShareholding[]
    ) {}
}
