import { ICompany } from 'app/shared/model/userdata/company.model';
import { IPerson } from 'app/shared/model/userdata/person.model';

export interface ICompanyShareholding {
    id?: number;
    percentage?: number;
    company?: ICompany;
    director?: IPerson;
}

export class CompanyShareholding implements ICompanyShareholding {
    constructor(public id?: number, public percentage?: number, public company?: ICompany, public director?: IPerson) {}
}
