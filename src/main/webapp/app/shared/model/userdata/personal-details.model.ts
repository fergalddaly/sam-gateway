import { IPerson } from 'app/shared/model/userdata/person.model';

export const enum MartialStatus {
    SINGLE = 'SINGLE',
    MARRIED = 'MARRIED'
}

export interface IPersonalDetails {
    id?: number;
    userId?: number;
    homeAddress?: string;
    ppsn?: string;
    martialStatus?: MartialStatus;
    spousesForename?: string;
    spousesSurname?: string;
    person?: IPerson;
}

export class PersonalDetails implements IPersonalDetails {
    constructor(
        public id?: number,
        public userId?: number,
        public homeAddress?: string,
        public ppsn?: string,
        public martialStatus?: MartialStatus,
        public spousesForename?: string,
        public spousesSurname?: string,
        public person?: IPerson
    ) {}
}
