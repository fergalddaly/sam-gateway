import { Moment } from 'moment';

export interface ICompanyCroDetails {
    id?: number;
    companyName?: string;
    companyNumber?: number;
    companyStatusCode?: number;
    companyStatusDesc?: string;
    companyStatusDate?: Moment;
    companyTypeCode?: number;
    companyRegDate?: Moment;
    compTypeDesc?: string;
    placeOfBusiness?: string;
    eircode?: string;
    companyAddr1?: string;
    companyAddr2?: string;
    companyAddr3?: string;
    companyAddr4?: string;
    lastArDate?: Moment;
    nextArDate?: Moment;
    lastAccDate?: Moment;
}

export class CompanyCroDetails implements ICompanyCroDetails {
    constructor(
        public id?: number,
        public companyName?: string,
        public companyNumber?: number,
        public companyStatusCode?: number,
        public companyStatusDesc?: string,
        public companyStatusDate?: Moment,
        public companyTypeCode?: number,
        public companyRegDate?: Moment,
        public compTypeDesc?: string,
        public placeOfBusiness?: string,
        public eircode?: string,
        public companyAddr1?: string,
        public companyAddr2?: string,
        public companyAddr3?: string,
        public companyAddr4?: string,
        public lastArDate?: Moment,
        public nextArDate?: Moment,
        public lastAccDate?: Moment
    ) {}
}
