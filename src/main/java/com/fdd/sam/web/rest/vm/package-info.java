/**
 * View Models used by Spring MVC REST controllers.
 */
package com.fdd.sam.web.rest.vm;
